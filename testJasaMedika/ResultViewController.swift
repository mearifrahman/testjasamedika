//
//  ResultViewController.swift
//  testJasaMedika
//
//  Created by arifrahman on 5/29/17.
//  Copyright © 2017 arifrahman. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    static func instantiate() -> ResultViewController {
        let controller = UIStoryboard.main.instantiateViewController(withIdentifier: "ResultVC") as! ResultViewController
        return controller
    }
    
    static func instantiateNav() -> UINavigationController {
        let nav = UIStoryboard.main.instantiateViewController(withIdentifier: "ResultVCN") as! UINavigationController
        return nav
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.setBackgroundImage(imageLayerForGradientBackground(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        tableView.dataSource = self
        tableView.delegate = self
        self.view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 140
        
        tableView.register(UINib(nibName: "ResultCell", bundle: nil), forCellReuseIdentifier: "ResultCell")
        
    }
    
}

extension ResultViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResultCell", for: indexPath) as! ResultCell
        if indexPath.row == 1 {
            cell.imageProfile.image = #imageLiteral(resourceName: "profile1")
            cell.labelName.text = "Elvis Swaniawski"
            cell.labelAddress.text = "Fisher Mountains"
        } else if indexPath.row == 2 {
            cell.imageProfile.image = #imageLiteral(resourceName: "profile2")
            cell.labelName.text = "Eleanore Stokes"
            cell.labelAddress.text = "Wunsch Passage"
        } else {
            cell.imageProfile.image = #imageLiteral(resourceName: "profile3")
            cell.labelName.text = "Muhammad Arif Rahman"
            cell.labelAddress.text = "Jalan Bawean"
        }
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //navigationController.push(navController, animated:true)
    }
}
