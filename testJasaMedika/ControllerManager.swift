//
//  ControllerManager.swift
//  testJasaMedika
//
//  Created by arifrahman on 5/29/17.
//  Copyright © 2017 arifrahman. All rights reserved.
//

import UIKit

class ControllerManager: NSObject {
    
    static func setRoot(controller: UIViewController) {
        UIApplication.shared.keyWindow?.rootViewController = controller
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
    }
    
    static func presentModalFromRight(controller: UIViewController, from parent:UIViewController, completion: (() -> Swift.Void)? = nil) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        parent.view.window?.layer.add(transition, forKey: nil)
        parent.present(controller, animated: false, completion: completion)
    }
    
    static func dismissFromLeft(controller: UIViewController, from parent:UIViewController, completion: (() -> Swift.Void)? = nil) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        parent.view.window?.layer.add(transition, forKey: nil)
        controller.dismiss(animated: false, completion: completion)
    }
    
}
