//
//  LoginViewController.swift
//  testJasaMedika
//
//  Created by arifrahman on 5/29/17.
//  Copyright © 2017 arifrahman. All rights reserved.
//

import UIKit
import SVProgressHUD

class LoginViewController: UIViewController {
    
    @IBOutlet weak var textFieldUsername: FloatLabelTextField!
    @IBOutlet weak var textFieldPassword: FloatLabelTextField!
    
    static func instantiate() -> LoginViewController {
        let controller = UIStoryboard.main.instantiateViewController(withIdentifier: "LoginVC") as! LoginViewController
        return controller
    }
    
    static func instantiateNav() -> UINavigationController {
        let nav = UIStoryboard.main.instantiateViewController(withIdentifier: "LoginVCN") as! UINavigationController
        return nav
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTransparentNavbar()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func buttonLoginPressed(_ sender: Any) {
        if textFieldUsername.text?.isEmpty == true || textFieldUsername.text == "" || textFieldPassword.text?.isEmpty == true || textFieldPassword.text == "" {
            SVProgressHUD.showError(withStatus: "Email atau Password masih kosong")
            SVProgressHUD.delayDismiss(delay: 1.0)
        } else if textFieldUsername.text! == "admin", textFieldPassword.text! == "admin" {
            let controller = SearchViewController.instantiate()
            self.navigationController?.pushViewController(controller, animated: true)
            //            textFieldUsername.resignFirstResponder()
            //            textFieldPassword.resignFirstResponder()
            //            SVProgressHUD.show()
            //            interactor.login(email: textFieldUsername.text!, password: textFieldPassword.text!, success: {
            //                // aksi sukses login
            //            }, failure: { error in
            //                SVProgressHUD.showError(withStatus: error.localizedDescription)
            //                SVProgressHUD.delayDismiss(delay: 1.0)
            //            })
        } else {
            SVProgressHUD.showError(withStatus: "Email atau Password salah!")
            SVProgressHUD.delayDismiss(delay: 1.0)
        }
    }
    
    func setTransparentNavbar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
}

extension UIStoryboard {
    static var main: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
}

import UIKit

extension UIView {
    
    func makeShadow(radius:CGFloat = 5) {
        self.layer.cornerRadius = radius
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 5
        self.layer.shadowColor = UIColor.black.cgColor
    }
    
    func makeRound(radius:CGFloat = 5) {
        self.layer.cornerRadius = radius
        //        self.clipsToBounds = true
    }
    
    func circlify() {
        let roundValue = frame.width / 2.0
        layer.cornerRadius = roundValue
        clipsToBounds = true
    }
    
    func setGradientBackground(firstColor: UIColor, secondColor: UIColor) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [firstColor.cgColor, secondColor.cgColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 1.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.size.width, height: self.frame.size.height)
        self.layer.insertSublayer(gradient, at: 0)
    }
}

extension SVProgressHUD {
    static func delayDismiss(delay: Double) {
        let dispatch = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: dispatch) {
            SVProgressHUD.dismiss()
        }
    }
}
