//
//  SearchViewController.swift
//  testJasaMedika
//
//  Created by arifrahman on 5/29/17.
//  Copyright © 2017 arifrahman. All rights reserved.
//

import UIKit
import SVProgressHUD

class SearchViewController: UIViewController {
    
    @IBOutlet weak var searchTextField: FloatLabelTextField!
    
    static func instantiate() -> SearchViewController {
        let controller = UIStoryboard.main.instantiateViewController(withIdentifier: "SearchVC") as! SearchViewController
        return controller
    }
    
    static func instantiateNav() -> UINavigationController {
        let nav = UIStoryboard.main.instantiateViewController(withIdentifier: "SearchVCN") as! UINavigationController
        return nav
    }
    
    @IBAction func buttonSearchPressed(_ sender: Any) {
        if searchTextField.text?.isEmpty == true || searchTextField.text == "" {
            SVProgressHUD.showError(withStatus: "Masukkan kata kunci")
            SVProgressHUD.delayDismiss(delay: 1.0)
        } else {
            let controller = ResultViewController.instantiate()
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func buttonLogoutPressed(_ sender: Any) {
        let controller = LoginViewController.instantiateNav()
//        self.navigationController?.pushViewController(controller, animated: true)
        self.present(controller, animated: true, completion: {
            ControllerManager.setRoot(controller: controller)
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.setBackgroundImage(imageLayerForGradientBackground(), for: UIBarMetrics.default)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
}

extension UIViewController {
    
    func setGradientLayerForBounds(firstColor: UIColor, secondColor: UIColor, bounds: CGRect) -> CAGradientLayer {
        let layer = CAGradientLayer()
        layer.frame = bounds
        layer.colors = [firstColor.cgColor, secondColor.cgColor]
        return layer
    }
    
    func imageLayerForGradientBackground() -> UIImage {
        var updatedFrame = self.navigationController?.navigationBar.bounds
        // take into account the status bar
        updatedFrame?.size.height += 20
        let layer = self.setGradientLayerForBounds(firstColor: ColorHelper.MAIN_GRADIENT_FIRST, secondColor: ColorHelper.MAIN_GRADIENT_SECOND,bounds: updatedFrame!)
        UIGraphicsBeginImageContext(layer.bounds.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
}
