//
//  AuthInteractor.swift
//  testJasaMedika
//
//  Created by arifrahman on 5/29/17.
//  Copyright © 2017 arifrahman. All rights reserved.
//

import UIKit
import SwiftyJSON

class AuthInteractor: BaseInteractor {
    
    func login(email: String, password: String, success: @escaping () -> (Void), failure: @escaping (NSError) -> (Void)) {
        service.login(email: email, password: password, success: { user in
            self.saveModel(data: user)
            success()
        }, failure: { error in
            failure(error)
        })
    }
    
    func logout(success: @escaping () -> (Void), failure: @escaping (NSError) -> (Void)) {
        service.logout(success: {
            success()
        }, failure: { error in
            failure(error)
        })
    }
    
}
