//
//  JasaMedikaCoreService.swift
//  testJasaMedika
//
//  Created by arifrahman on 5/29/17.
//  Copyright © 2017 arifrahman. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

extension JasaMedikaCoreService {
    
    func login(email: String, password: String, success: @escaping (User) -> (Void), failure: @escaping (NSError) -> (Void)) {
        let parameters: [String: AnyObject] = [
            "userName": email as AnyObject,
            "password": password as AnyObject,
            ]
        let request = manager.request(home + "/user/authentication", method: .post, parameters: parameters)
        request.responseJSON { response in
            switch response.result {
            case let .success(value):
                let json = JSON(value)
                let codeJson = json["status"]
                let resultJson = json["result"]
                let userJson = resultJson["user"]
                if codeJson.exists() && codeJson.stringValue == "20" {
                    if let user = User.with(json: userJson) {
                        success(user)
                    } else {
                        let error = NSError(domain: "jasamedika", code: -1, userInfo: [NSLocalizedDescriptionKey: "Something went wrong!"])
                        print(error.localizedDescription)
                        failure(error)
                    }
                } else {
                    let error = NSError(domain: "jasamedika", code: -1, userInfo: [NSLocalizedDescriptionKey: "\(json["message"].object)"])
                    print(error.localizedDescription)
                    failure(error)
                }
            case let .failure(error):
                print(error.localizedDescription)
                failure(error as NSError)
            }
        }
        
    }
}
