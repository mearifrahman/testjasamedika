//
//  ResultCell.swift
//  testJasaMedika
//
//  Created by arifrahman on 5/29/17.
//  Copyright © 2017 arifrahman. All rights reserved.
//

import UIKit

class ResultCell: UITableViewCell {
    
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageProfile.circlify()
        viewContainer.makeRound()
        viewContainer.makeShadow()
    }
    
}
